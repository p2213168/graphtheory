import Graph
import BFSgraph
import random
import Labellisation
import Embed

file = "CL-1000-2d0-trial2/CL-1000-2d0-trial2.edges"

g = Graph.uploadGraph(file)
#g = Graph.connectGraph(g)

# Print medium degree
print("Medium degree: ", Graph.mediumDegree(g))

# Print LdmNode
print("LdmNode: ", Graph.LdmNode(g, 1))

print(Graph.isConnectedGraph(g), " \n")
print(g.number_of_nodes())

print("_______________ extracting a subgraph with BFS Algorithm ___________")

# Getting a list of possible starting nodes and then affect one of them to nodeStart
nodeStart = random.choice(Graph.LdmNode(g,3))
print(nodeStart)
print(g.degree(nodeStart))


#extract the subGraph and display it
subGraph = BFSgraph.BFSsubgraph(g,nodeStart,100)




subGraph = Graph.EigenvectorCentrality(subGraph)
subGraph = Graph.ClosenessCentrality(subGraph)
subGraph = Graph.ClusteringCoefficient(subGraph)
subGraph = Graph.BetweennessCentrality(subGraph)
subGraph = Graph.degrePropertie(subGraph)

Graph.displayNodeAttributes(subGraph)



#graph = Labellisation.normalize_features(g)

Graph.displayNodeAttributes(subGraph)


graph = Labellisation.normalize_features(subGraph)
Graph.displayNodeAttributes(graph)

print(Graph.top_nodes(graph,'average', num_top=10))

labels = Labellisation.labelNodeByAverage(subGraph)


# print nodes that are critical
for node, label in labels.items():
    if label == 1:
        print(node)

#degree, eigenvectorcentrality, betweeness centrality


Embed.deepWalk(subGraph)

Embed.apply_pca('embeddings')

Embed.displayEmbedding('ACP_Reduction', labels)

Graph.visualizeGraph(graph)

Embed.node2Vec(subGraph)

Embed.apply_pca('embeddings')

Embed.displayEmbedding('ACP_Reduction', labels)

Graph.visualizeGraph(graph)